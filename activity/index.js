const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', printFirstAndLastName);
txtLastName.addEventListener('keyup', printFirstAndLastName);

function printFirstAndLastName(event) {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
};

// or 

/* 
    function printFirstAndLastName(event) {
    let firstName = txtFirstName.value
    let lastName = txtLastName.value

    spanFullName.innerHTML = `${firstName} ${lastName}`
};

*/