// alert('Hello World!')

console.log(document.querySelector("#txt-first-name"));

// console.log(document);

/* 
    Alternative methods that we use aside from querySelector in retrieving elements

    Syntax:
        document.getElementById()
        document.getElementByClassName()
        document.getElementByTagName()
*/

// DOM Manipulation

const txtFirstName = document.querySelector("#txt-first-name");

const spanFullName = document.querySelector("#span-full-name");

// console.log(txtFirstName);
// console.log(spanFullName);

/* 
    Event: 
        click, hover, keypress, keyup and many others.
    Event Listeners:
        Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task. 

    Syntax:
        selectedElement.addEventListener('event', function);

*/

// txtFirstName.addEventListener('keyup', (event) => {
//     spanFullName.innerHTML = txtFirstName.value
// });

// or

txtFirstName.addEventListener('keyup', printFirstName)

function printFirstName(event) {
    spanFullName.innerHTML = txtFirstName.value
};

/* 
    innerHTML - is a property of an element which considers all the children of the selected element as a string. 

    .value of the input text field 
*/

txtFirstName.addEventListener('keyup', (event) => {
    console.log(event)
    console.log(event.target)
    console.log(event.target.value)
});

const labelFirstName = document.querySelector("#label-txt-name");

labelFirstName.addEventListener('click', (e) => {
    console.log(e)
    alert("You clicked the First Name Label")
});

/* 
    1. S47 Activity - 40 minutes
    2. Solution Discussion -10 minutes
    3. JavaScript - Reactive DOM with JSON - 60 minutes
    4. S48 Activity - 60 minutes
    5. Solution Discussion: 10 minutes
    6. JavaScript - Reactive DOM with Fetch (first half) - 30 minutes
*/